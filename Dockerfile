FROM ruby:3.2 AS base
FROM base AS builder
FROM builder AS runner
FROM runner
ADD ./public/* /public/
RUN gpasswd -a www-data www-data && \
chmod 750 /public && \
chown www-data:www-data /public && \
chmod 640 /public/* && \
chmod 700 /public
RUN gem install bundler && \
bundle install && \
bundle exec jekyll build
USER www-data